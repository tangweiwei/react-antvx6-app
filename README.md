# react-antvx6-app
基于 react 和 antv x6 制作一款可视化拖拽流程拓扑图
项目主要依赖模块:
"react": "^17.0.2",
"antd": "^4.16.8",
"react-dom": "^17.0.2",
"react-router-dom": "^5.2.0",
"typescript": "^4.3.5",
"@ant-design/icons": "^4.6.2",
"@antv/x6": "^1.24.8",
"@antv/x6-react-components": "^1.1.13",

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:3000
npm run start

# build for production with minification
npm run build
