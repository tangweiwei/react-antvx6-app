import React, { useEffect, useState } from 'react'
import './index.scss'
import { GithubOutlined, UserOutlined } from '@ant-design/icons'
import ToolBar from './components/ToolBar'
import RightDrawer from './components/RightDrawer'
import FlowGraph from './Graph'
import { useHistory } from 'react-router-dom'

const Index = () => {
  const history = useHistory();
  const [ isReady, setIsReady ] = useState(false) // 是否初始化完成，渲染 toolbar
  const [ isRightDrawer, setIsRightDrawer ] = useState(false)
  const [ selectCell, setSelectCell ] = useState({})

  useEffect(() => {
    const graph = FlowGraph.init()
    setIsReady(true)

    // 节点选中事件
    graph.on('selection:changed', (args) => {
      args.added.forEach((cell: any) => {
        const shape = cell.store.data.shape

        if (shape !== 'edge') {
          setSelectCell(cell)
          setIsRightDrawer(true)
        }
      })
    })
    graph.on('blank:click', () => {
      setIsRightDrawer(false)
    })

    const resizeFn = () => {
      const { width, height } = getContainerSize()
      graph.resize(width, height)
    }
    resizeFn()

    window.addEventListener('resize', resizeFn)
    return () => {
      window.removeEventListener('resize', resizeFn)
    }
  }, [])

  const getContainerSize = () => {
    return {
      width: document.body.offsetWidth - 214,
      height: document.body.offsetHeight - 95,
    }
  }

  const openGithub = () => {
    window.open(
      'https://www.ifrontend.net/',
      '_blank',
    )
  }

  const openUser = () => {
    history.push('/about')
  }

  const closeRightDrawer = () => {
    setIsRightDrawer(false)
  }

  return (
    <div className="antv-x6">
      <div className="header">
        <span className="txt">React-AntvX6-App</span>
        <span className="icon">
          <UserOutlined onClick={openUser} title="关于我们"></UserOutlined>
          <GithubOutlined onClick={openGithub} title="Github"></GithubOutlined>
        </span>
      </div>
      <div className="toolbar">
        {isReady && <ToolBar />}
      </div>
      {isRightDrawer && <RightDrawer selectCell={selectCell} close={closeRightDrawer} />}
      <div className="content">
        <div id="stencil" className="shapes"></div>
        <div id="container"></div>
        <div id="minimap" className="minimap"></div>
      </div>
    </div>
  )
}

export default Index