import React, { Component } from 'react';
import './about.scss'

export default class About extends Component {
  render() {
    return (
      <div className="about">
        <h2>关于我们</h2>
        <div className="cnb">
          <ul>
            <li>姓名：伍哥的传说</li>
            <li>地址：<a href="https://www.ifrontend.net/" target="_blank">https://www.ifrontend.net/</a></li>
            <li>邮箱：844475003@qq.com</li>
          </ul>
        </div>
      </div>
    )
  }
}