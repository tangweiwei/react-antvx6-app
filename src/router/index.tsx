import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import index from '../views/index'
import about from '../views/about'

export default class RouteConfig extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={index}></Route>
          <Route path="/about" exact component={about}></Route>
        </Switch>
      </BrowserRouter>
    )
  }
}